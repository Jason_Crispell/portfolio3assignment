﻿namespace CrispellJason_Assign2_ex2_ContactList
{
    partial class ImageModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageModal));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnGolem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGoblin = new System.Windows.Forms.Button();
            this.btnDragon = new System.Windows.Forms.Button();
            this.btnBandit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Bandit.png");
            this.imageList1.Images.SetKeyName(1, "Dragon.png");
            this.imageList1.Images.SetKeyName(2, "Goblin.png");
            this.imageList1.Images.SetKeyName(3, "Golem.png");
            // 
            // btnGolem
            // 
            this.btnGolem.AutoSize = true;
            this.btnGolem.ImageIndex = 3;
            this.btnGolem.ImageList = this.imageList1;
            this.btnGolem.Location = new System.Drawing.Point(166, 127);
            this.btnGolem.Name = "btnGolem";
            this.btnGolem.Size = new System.Drawing.Size(87, 70);
            this.btnGolem.TabIndex = 3;
            this.btnGolem.UseVisualStyleBackColor = true;
            this.btnGolem.Click += new System.EventHandler(this.btnGolem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Choose an Icon";
            // 
            // btnGoblin
            // 
            this.btnGoblin.AutoSize = true;
            this.btnGoblin.ImageIndex = 2;
            this.btnGoblin.ImageList = this.imageList1;
            this.btnGoblin.Location = new System.Drawing.Point(15, 127);
            this.btnGoblin.Name = "btnGoblin";
            this.btnGoblin.Size = new System.Drawing.Size(87, 70);
            this.btnGoblin.TabIndex = 5;
            this.btnGoblin.UseVisualStyleBackColor = true;
            this.btnGoblin.Click += new System.EventHandler(this.btnGoblin_Click);
            // 
            // btnDragon
            // 
            this.btnDragon.AutoSize = true;
            this.btnDragon.ImageIndex = 1;
            this.btnDragon.ImageList = this.imageList1;
            this.btnDragon.Location = new System.Drawing.Point(166, 35);
            this.btnDragon.Name = "btnDragon";
            this.btnDragon.Size = new System.Drawing.Size(87, 70);
            this.btnDragon.TabIndex = 6;
            this.btnDragon.UseVisualStyleBackColor = true;
            this.btnDragon.Click += new System.EventHandler(this.btnDragon_Click);
            // 
            // btnBandit
            // 
            this.btnBandit.AutoSize = true;
            this.btnBandit.ImageIndex = 0;
            this.btnBandit.ImageList = this.imageList1;
            this.btnBandit.Location = new System.Drawing.Point(15, 35);
            this.btnBandit.Name = "btnBandit";
            this.btnBandit.Size = new System.Drawing.Size(87, 70);
            this.btnBandit.TabIndex = 7;
            this.btnBandit.UseVisualStyleBackColor = true;
            this.btnBandit.Click += new System.EventHandler(this.btnBandit_Click);
            // 
            // ImageModal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 209);
            this.Controls.Add(this.btnBandit);
            this.Controls.Add(this.btnDragon);
            this.Controls.Add(this.btnGoblin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGolem);
            this.Name = "ImageModal";
            this.Text = "ImageModal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnGolem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGoblin;
        private System.Windows.Forms.Button btnDragon;
        private System.Windows.Forms.Button btnBandit;
    }
}