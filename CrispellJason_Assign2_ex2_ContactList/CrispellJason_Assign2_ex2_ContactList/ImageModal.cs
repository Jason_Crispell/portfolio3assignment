﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispellJason_Assign2_ex2_ContactList
{
    public partial class ImageModal : Form
    {
        public EventHandler imageInput;
        public int imageIndex;

        public ImageModal()
        {
            InitializeComponent();
        }

        private void btnBandit_Click(object sender, EventArgs e)
        {
            imageIndex = 0;
            imageInput(this, new EventArgs());
            this.Close();
        }

        private void btnDragon_Click(object sender, EventArgs e)
        {
            imageIndex = 1;
            imageInput(this, new EventArgs());
            this.Close();
        }

        private void btnGoblin_Click(object sender, EventArgs e)
        {
            imageIndex = 2;
            imageInput(this, new EventArgs());
            this.Close();
        }

        private void btnGolem_Click(object sender, EventArgs e)
        {
            imageIndex = 3;
            imageInput(this, new EventArgs());
            this.Close();
        }
    }
}
