﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispellJason_Assign2_ex2_ContactList
{
    public partial class Form1 : Form
    {
        Contact newContact;

        public Form1()
        {
            InitializeComponent();

            newContact = new Contact(txtFirst.Text, txtLast.Text, txtPhone.Text, txtEmail.Text);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Validate first and last name
            if (!((validateName(txtFirst.Text)) && (validateName(txtLast.Text))))
            {
                MessageBox.Show("Invalid name");
                return;
            }
            
            // Validate phone
            if (!(validatePhone(txtPhone.Text)))
            {
                return;
            }
            if (!(validateEmail(txtEmail.Text)))
            {
                return;
            }

            // Assign text values to contact object.
            newContact.FirstName = txtFirst.Text;
            newContact.LastName = txtLast.Text;
            newContact.Phone = txtPhone.Text;
            newContact.Email = txtEmail.Text;
            newContact.ImageIndex = btnIcon.ImageIndex;

            
            ListViewItem contactItem = new ListViewItem();
            contactItem.Tag = newContact;
            contactItem.ImageIndex = newContact.ImageIndex;
            contactItem.Text = newContact.FirstName + " " + newContact.LastName;
            listView1.Items.Add(contactItem);

            // Instantiate a new contact to make sure that we don't keep adding
            // the same one to the list.
            newContact = new Contact(txtFirst.Text, txtLast.Text, txtPhone.Text, txtEmail.Text);

            // Remove the old contact if we're updating an existing contact
            if (listView1.SelectedItems.Count > 0)
            {
                listView1.Items.Remove(listView1.SelectedItems[0]);
            }

            clearForm();
        }

        private bool validateEmail(string email)
        {
            char[] emailArray = email.ToArray<char>();

            // Check for the @ symbol
            if (!(emailArray.Contains<char>('@')))
            {
                MessageBox.Show("Your email address must include the @ symbol.");
                return false;
            }

            // Check for .
            if (!(emailArray.Contains<char>('.')))
            {
                MessageBox.Show("Invalid Email address.");
                return false;
            }


            string[] emailStringArray = email.Split('@');

            // Make sure there is text before and after the @
            if (emailStringArray.Count() < 2)
            {
                MessageBox.Show("Invalid Email address.");
                return false;
            }
            if ((emailStringArray[0] == "") || (emailStringArray[1] == ""))
            {
                MessageBox.Show("Invalid Email address.");
                return false;
            }

            // Make sure that there's a period after the @ symbol.
            if (!(emailStringArray[1].Contains(".")))
            {
                MessageBox.Show("Invalid email address");
                return false;
            }


            return true;
        }

        private bool validatePhone(string phone)
        {
            char[] phoneArray = phone.ToArray<char>();

            // Check length
            if (phoneArray.Count() != 12)
            {
                MessageBox.Show("Invalid Phone Length. Use this format: 407-123-4567");
                return false;
            }
            for (int i = 0; i< 12; i++)
            {
                // Check for the dashes
                if ((i==3) || (i == 7))
                {
                    if (phoneArray[i] != '-')
                    {
                        MessageBox.Show("Invalid Phone Format. Use this format: 407-123-4567");
                        return false;
                    }
                }
                else
                {
                    // Make sure everything else is a digit.
                    if (!((phoneArray[i] == '1') || (phoneArray[i] == '2') || (phoneArray[i] == '3') || (phoneArray[i] == '4') || (phoneArray[i] == '5') || (phoneArray[i] == '6') || (phoneArray[i] == '7') || (phoneArray[i] == '8') || (phoneArray[i] == '9') || (phoneArray[i] == '0')))
                    {
                        MessageBox.Show("Invalid Phone Number. Use this format: 407-123-4567");
                        return false;
                    }
                }

            }
            return true;
        }

        private bool validateName(string name)
        {
            // Make sure no numbers in name.
            if ((name.Contains("0")) || (name.Contains("1")) || (name.Contains("2")) || (name.Contains("3")) || (name.Contains("4")) || (name.Contains("5")) || (name.Contains("6")) || (name.Contains("7")) || (name.Contains("8")) || (name.Contains("9")))
            {
                return false;
            }
            else if (name == "")
            {
                return false;
            }
            return true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            clearForm();

            if (listView1.SelectedItems.Count > 0)
            {
                listView1.Items.Remove(listView1.SelectedItems[0]);
            }
        }

        private void clearForm()
        {
            txtEmail.Clear();
            txtFirst.Clear();
            txtLast.Clear();
            txtPhone.Clear();
            btnIcon.ImageIndex = 0;
        }

        private void btnIcon_Click(object sender, EventArgs e)
        {
            ImageModal newModal = new ImageModal();

            // Subscribe the EventHandler so we can get the user selection back
            // from the modal dialog.
            newModal.imageInput += setImageIndex;

            newModal.ShowDialog();
        }

        private void setImageIndex(object sender, EventArgs e)
        {
            if (sender != null)
            {
                ImageModal senderForm = sender as ImageModal;

                newContact.ImageIndex = senderForm.imageIndex;
                btnIcon.ImageIndex = senderForm.imageIndex;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Populate text controls with contact data when contact is selected
            if (listView1.SelectedItems.Count > 0)
            {
                Contact contact = listView1.SelectedItems[0].Tag as Contact;

                txtFirst.Text = contact.FirstName;
                txtLast.Text = contact.LastName;
                txtPhone.Text = contact.Phone;
                txtEmail.Text = contact.Email;
                btnIcon.ImageIndex = contact.ImageIndex;
            }
        }

        private void chkLargeIcons_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLargeIcons.Checked == false)
            {
                listView1.View = View.SmallIcon;
            }
            else listView1.View = View.LargeIcon;
        }
    }
}
