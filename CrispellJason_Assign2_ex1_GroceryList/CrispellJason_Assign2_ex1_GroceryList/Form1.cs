﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CrispellJason_Assign2_ex1_GroceryList
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSwap_Click(object sender, EventArgs e)
        {
            // Check to see which list we're looking at. Add to other list, then
            // remove from original list to complete the swap.

            if (lstHave.SelectedIndex >= 0)
            {
                lstNeed.Items.Add(lstHave.SelectedItem);
                lstHave.Items.RemoveAt(lstHave.SelectedIndex);
            }
            else if (lstNeed.SelectedIndex >= 0)
            {
                lstHave.Items.Add(lstNeed.SelectedItem);
                lstNeed.Items.RemoveAt(lstNeed.SelectedIndex);
            }
            else
                MessageBox.Show("Select an item to swap.");
        }

        private void btnHave_Click(object sender, EventArgs e)
        {
            // Check to make sure there isn't a blank entry.
            if (checkText())

            // Add the item to the Have list.
            {
                lstHave.Items.Add(txtItem.Text);
            }

            // Clear and select the textbox control
            txtItem.Text = "";
            txtItem.Select();
        }

        private void btnNeed_Click(object sender, EventArgs e)
        {
            // Check to make sure there isn't a blank entry.
            if (checkText())

            // Add the item to the Need list.
            {
                lstNeed.Items.Add(txtItem.Text);
            }

            // Clear and select the textbox control
            txtItem.Text = "";
            txtItem.Select();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Check to see which list is selected, then delete.
            if (lstHave.SelectedIndex >= 0)
            {
                lstHave.Items.RemoveAt(lstHave.SelectedIndex);
            }
            else if (lstNeed.SelectedIndex >= 0)
            {
                lstNeed.Items.RemoveAt(lstNeed.SelectedIndex);
            }
            else
                MessageBox.Show("Select an item to delete.");
           
            
        }

        private bool checkText()
        {
            if (txtItem.Text == "")
            {
                MessageBox.Show("You must write an item description.");
                return false;
            }
            return true;
        }

        private void lstHave_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Ensure only one listbox is active at a time
            if (lstHave.SelectedIndex >= 0)            
            lstNeed.SelectedIndex = -1;
        }

        private void lstNeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Ensure only one listbox is active at a time
            if (lstNeed.SelectedIndex >= 0)
                lstHave.SelectedIndex = -1;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.DefaultExt = "txt";

            // Check to see if user clicked OK
            if (DialogResult.OK == dlg.ShowDialog())
            {
                // Configure settings

                // Instantiate the writer
                using (StreamWriter writer = new StreamWriter(dlg.FileName))
                {
                    // Stringbuilder to hold all the data
                    StringBuilder builder = new StringBuilder();
                    builder.Append("Have: ");
                    if (lstHave.Items.Count == 0)
                    {
                        builder.Append("No items listed.");
                        builder.AppendLine();
                    }
                    else
                    {
                        foreach (object item in lstHave.Items)
                        {
                            builder.Append(item + " ");
                        }
                        builder.AppendLine();
                    }

                    builder.Append("Need: ");
                    if (lstNeed.Items.Count == 0)
                    {
                        builder.Append("No items listed.");
                        builder.AppendLine();
                    }
                    else
                    {
                        foreach (object item in lstNeed.Items)
                        {
                            builder.Append(item + " ");
                        }
                        builder.AppendLine();
                    }

                    // write the builder
                    writer.Write(builder.ToString());
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
