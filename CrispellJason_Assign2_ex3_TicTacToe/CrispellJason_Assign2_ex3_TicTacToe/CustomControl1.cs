﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispellJason_Assign2_ex3_TicTacToe
{
    public partial class CustomControl1 : Control


    {
        public Dictionary<Point, Point> changedTilesDict = new Dictionary<Point, Point>();


        // Size of each tile in pixels
        Size tileSize = new Size(64, 64);

        // Map with points as a 2-dimensional array
        // We'll start with a default size of 3x3
        public Point[,] map = new Point[3, 3];

        // Here we'll add the mapSize along with a summary and
        // some additional attributes
        /// <summary>
        /// Gets and sets the size of the map in tiles
        /// </summary>
        // Now, we'll add the attributes
        [Category("Map")]
        [Description("The size of the map in tiles.")]
        [DefaultValue(typeof(Size), "3, 3")]
        public Size MapSize
        {
            get { return new Size(map.GetLength(0), map.GetLength(1)); }
            set
            {
                map = new Point[value.Width, value.Height];
    

                // When we get a new map size, we redraw the control
                Invalidate();
            }
        }

        public Point this[int x, int y]
        {
            // return the current map array
            get { return map[x, y]; }
            set { map[x, y] = value; }
        }

        // The initial image for a selected tile
        Point selectedTile = new Point(1, 1);

        public Point SelectedTile
        {
            get
            {
                return selectedTile;
            }

            set
            {
                selectedTile = value;
            }
        }

        public Bitmap tileImage = Properties.Resources.blank;
        public Bitmap tileImageX = Properties.Resources.x;
        public Bitmap tileImageO = Properties.Resources.OImage;

        Point mouseLoc = new Point(0, 0);

        public CustomControl1()
        {
            InitializeComponent();
        }

        [Category("Map")]
        [Description("The size of a single tile in pixels.")]
        public Size TileSize
        {
            get
            {
                return tileSize;
            }

            set
            {
                tileSize = value;
                Invalidate();
            }
        }


        public Bitmap TileImage
        {
            get
            {
                return tileImage;
            }

            set
            {
                tileImage = value;


                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {

            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    // Create a rectangle that represents where we will be
                    // drawing in the map.
                    // Create the rectangle
                    Rectangle destRect = Rectangle.Empty;
                    // Check the dimensions of hte tileSize variable
                    // x and y of where to start drawing the rectangle
                    destRect.X = x * tileSize.Width;
                    destRect.Y = y * tileSize.Height;
                    // the size of the rectangle
                    destRect.Size = tileSize;


                    // Now, we'll create the rectangle that will be used
                    // for the drawing
                    Rectangle srcRect = Rectangle.Empty;
                    // If the tile has been selected, the selected tile
                    // index will be stored. Otherwise, the default 
                    // image will be stored. Becasue every map Point is
                    // defaulted to 0, 0, this will grab the 0, 0 tile
                    // from the tileset unless the value in the Point
                    // has changed
                    srcRect.X = map[x, y].X * tileSize.Width;
                    srcRect.Y = map[x, y].Y * tileSize.Height;
                    srcRect.Size = tileSize;

                    // draw the source image to the destination
                    if (tileImage != null)
                        pe.Graphics.DrawImage(tileImage, destRect, srcRect, GraphicsUnit.Pixel);

                    // draw a rectnagle around this image to create the grid
                    pe.Graphics.DrawRectangle(Pens.Black, destRect);


                }
            }

            base.OnPaint(pe);
        }

        private void CustomControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mouseLoc = e.Location;

            // convert the pixel location to the grid coordinate
            // on our map
            int x = mouseLoc.X / tileSize.Width;
            int y = mouseLoc.Y / tileSize.Height;

            // Store the selected tile location in this map location
            // so we know what to drawin the invalidion of the control
            map[x, y] = selectedTile;



            // redraw once the new tile is selected
            Invalidate();
        }
    }
    }

