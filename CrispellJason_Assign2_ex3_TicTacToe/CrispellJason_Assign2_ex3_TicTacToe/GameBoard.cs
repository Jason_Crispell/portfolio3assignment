﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispellJason_Assign2_ex3_TicTacToe
{
    public partial class GameBoard : UserControl
    {
        // This holds the X's and 0's. 
        public Dictionary<int, string> ticTacToeDict = new Dictionary<int, string>();
        int dictionaryCounter = 0;
        Point mouseLoc = new Point(0, 0);

        public string xOrO = "x";


        public Bitmap tileImage = Properties.Resources.blank;
        public Bitmap tileImageX = Properties.Resources.x;
        public Bitmap tileImageO = Properties.Resources.OImage;

        public GameBoard()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    // Create tic tac toe grid
                    Rectangle destRect = Rectangle.Empty;

                    destRect.X = x * 64;
                    destRect.Y = y * 64;

                    // the size of the rectangle
                    destRect.Size = new Size(64, 64);

                    // Checks to see if the dictionary contains a listing for an 
                    // X or O. If not, draws a blank tile.
                    try
                    {
                        if (ticTacToeDict[dictionaryCounter] == "x")
                        {
                            pe.Graphics.DrawImage(tileImageX, destRect);
                        }
                        else if (ticTacToeDict[dictionaryCounter] == "o")
                        {
                            pe.Graphics.DrawImage(tileImageO, destRect);
                        }
                        else
                        {
                            pe.Graphics.DrawImage(tileImage, destRect);
                        }
                    }
                    catch (Exception e)
                    {
                        pe.Graphics.DrawImage(tileImage, destRect);

                    }



                    // draw a black outline to make the grid.
                    pe.Graphics.DrawRectangle(Pens.Black, destRect);

                    // This lets us know where in the dictionary we are.
                    dictionaryCounter++;
                }
            }

            // reset to beginning of dictionary
            dictionaryCounter = 0;


        }

        private void GameBoard_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mouseLoc = e.Location;


            // Convert from pixels to tiles
            int x = mouseLoc.X / 64;
            int y = mouseLoc.Y / 64;

            // Null check to prevent exceptions
            if (!ticTacToeDict.ContainsKey(x + (y * 3)))
            {
                // The dictionary key is an index number for the grid, 0-9 counting
                // left to right and top to bottom

                ticTacToeDict.Add(x + (y * 3), xOrO);
                

                // this is so X and O entries alternate
                if (xOrO == "x")
                {
                    xOrO = "o";
                }
                else
                {
                    xOrO = "x";
                }
            }
            else
            {
                MessageBox.Show("That spot has already been taken!");
            }


            Invalidate();
            CheckForWinner(ticTacToeDict);



        }

        private void CheckForWinner(Dictionary<int, string> ticTacToeDict)
        {
            // check for top row winner
            CheckRow(0, 1, 2);
            // middle
            CheckRow(3, 4, 5);
            // bot
            CheckRow(6, 7, 8);

            // check left column
            CheckRow(0, 3, 6);
            // middle
            CheckRow(1, 4, 7);
            // bot
            CheckRow(2, 5, 8);

            // check diagonals
            CheckRow(0, 4, 8);
            CheckRow(6, 4, 2);




        }

        private void CheckRow(int v1, int v2, int v3)
        {
            try
            {
                // Make sure all 3 in a row are the same.
                if ((ticTacToeDict[v1] == ticTacToeDict[v2]) && ticTacToeDict[v2] == ticTacToeDict[v3])
                {
                    MessageBox.Show(ticTacToeDict[v1].ToUpper() + " wins!");
                    ticTacToeDict.Clear();
                    Invalidate();
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
