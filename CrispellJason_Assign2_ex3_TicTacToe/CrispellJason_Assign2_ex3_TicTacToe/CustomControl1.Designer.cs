﻿namespace CrispellJason_Assign2_ex3_TicTacToe
{
    partial class CustomControl1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CustomControl1
            // 
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.CustomControl1_MouseDoubleClick);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
